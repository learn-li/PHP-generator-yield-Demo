<?php

require_once __DIR__ . '/yieldFunctions.php';

function yield_from_func1()
{
    // yield from ;            // Parse error: syntax error, unexpected ';'
}

function yield_from_func2()
{
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield from array(1, 2, 3);
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
}

function yield_from_func3()
{
    yield from 'test';
}

function yield_from_func4($bo = false)
{
    if ($bo) {
        yield from array(1, 2);
    }
}

function yield_from_func5()
{
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    $re = yield from array(1, 2, 3);
    echo 'run to code line: ' . __LINE__ . ". \t";
    echo 'get re: ';
    var_export($re);
    echo PHP_EOL;
}

function yield_from_func6()
{
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    list($re1, $re2, $re3) = yield from array(1, 2, 3);
    echo 'run to code line: ' . __LINE__ . ". \t";
    echo 'var_dump re1, re2, re3 : ';
    var_dump($re1, $re2, $re3);
    echo PHP_EOL;
}

function yield_from_func7()
{
    yield from false_yield_func1();
}

function yield_from_func8()
{
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
    yield from yield_func20();
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
}

function yield_from_func9()
{
    yield from yield_func9();
}

function yield_from_func10()
{
    $arr = array('aa','bb','cc');
    $re = yield 'func10';
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
    $re = yield from yield_from_func8();
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', var_export re: ';
    var_export($re);
    echo PHP_EOL;
    return 'from 10';
}

function yield_from_func11()
{
    $arr = array('aa','bb','cc');
    yield 'func11';
    $re = (yield from yield_from_func10());
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', var_export re: ';
    var_export($re);
    $re = (yield from yield_func10());
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', var_export re: ';
    var_export($re);
    echo PHP_EOL;
}

function yield_from_func13()
{
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
    $re = yield from yield_func21();
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', var_export re: ';
    var_export($re);
    echo PHP_EOL;
}

function yield_from_func14()
{
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
    $re = yield from yield_func22();
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', var_export re: ';
    var_export($re);
    echo PHP_EOL;
}